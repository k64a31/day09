<?php
    session_start();
    $QA = [
			[
			"question" => "7x1",
			"choice" => ["7", "14", "21", "71"]
			],
			[
      "question" => "7x2",
      "choice" => ["14", "17", "21", "71"]
      ],
			[
      "question" => "7x3",
      "choice" => ["21", "70", "71", "72"]
      ],
      [
      "question" => "7x4",
      "choice" => ["28", "70", "71", "72"]
      ],
      [
      "question" => "7x5",
      "choice" => ["35", "70", "71", "72"]
      ],
      [
      "question" => "7x6",
      "choice" => ["42", "71", "72", "73"]
      ],
      [
      "question" => "7x7",
      "choice" => ["49", "71", "72", "73"]
      ],
      [
      "question" => "7x8",
      "choice" => ["56", "71", "72", "73"]
      ],
      [
      "question" => "7x9",
      "choice" => ["63", "71", "72", "73"]
      ],
      [
      "question" => "7x10",
      "choice" => ["70", "71", "72", "73"]
      ]
		];
		$ans = array_fill(0,count($QA),0);
    $trueAnswer = array("0", "0", "0", "0", "0","0", "0", "0", "0", "0");

    if (isset($_POST['submit'])){
			for ($i = 0; $i < 5; $i++) {
				$ques_i = 'Câu_' . ($i+1);
				$ans[$i] = $_POST[$ques_i];
			}
			setcookie('QA', json_encode($QA));
			setcookie('ans', json_encode($ans));
      setcookie('trueAnswer', json_encode($trueAnswer));
			
      header('Location: '.'QA2.php');
		}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Past 1</title>
</head>
<div class ="question">
        <div class="title">Bảng cửu chương 7</div>
        <form action="" method="post">
            <?php
            for ($i = 0; $i < 5; $i++) {
              $ques_i = 'Câu ' . ($i+1);
              echo '<div id="' . $ques_i . '">';
              
              echo '<label><b>' . $ques_i . ':  </b>' . $QA[$i]["question"] . '</label><br>';
              $choices = '<input type="radio"  name="' . $ques_i . '" value="0">'. $QA[$i]["choice"][0] .' <br>
              <input type="radio"  name="' . $ques_i . '" value="1">'. $QA[$i]["choice"][1] .'<br>
              <input type="radio"  name="' . $ques_i . '" value="2">'. $QA[$i]["choice"][2] .'<br>
              <input type="radio"  name="' . $ques_i . '" value="3">'. $QA[$i]["choice"][3];
              echo $choices;
              echo '</div>';
            
            }
      
            ?>
            <br>
            <button name="submit" id="button-submit" class="button-submit" type="submit">Next</button>
        </form>  
    </div>

</body>
</html>