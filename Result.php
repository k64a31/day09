<?php
    $QA = json_decode($_COOKIE['QA'], true);
    $ans = json_decode($_COOKIE['ans'], true);
    $trueAnswer = json_decode($_COOKIE['trueAnswer'], true);
    $res = 0;
    for ($i = 0; $i < count($QA); $i++) {

        if ($ans[$i] == $trueAnswer[$i]){
            $res = $res + 1;
        }
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
</head>
<body>
<div class ="result">
        <?php
        echo "Bạn được " . $res . " điểm." ."</br>";
        if ($res < 4) {
            echo "Bạn quá kém, cần ôn tập thêm";
        }
        elseif ($res < 7) {
            echo "Cũng bình thường";
        }
        else {
            echo "Sắp sửa làm được trợ giảng lớp PHP";
        }
        ?>

    </div>

</body>
</html>